# Data Scientist Template
Data Scientists project structure template for research. It is assumed that each researcher will create their own project according to the specified template.

## Requirements to use the cookiecutter template
- `Python`>=3.11
- [`Cookiecutter Python package`][cookiecutter_docs]>= 2.4
- `Git` >= 2.42

## Getting Started
It is recommended to use [`pixi`][pixi_docs] to manage dependencies and create virtual environments. You can find installation instructions in `{{ cookiecutter.repo_name }}/README.md`.

Create a virtual environment:
```bash
pixi init
```

Install `python`, `cookiecutter` and `git`:
```bash
pixi add "python>=3.11" "cookiecutter>=2.4" "git>=2.42"
```

To start a new project, run:
```bash
cookiecutter https://gitlab.com/ml.devs/templates/data-scientist
```

## Template options
- **author_name**: "John Doe"
- **author_email**: "doe@john.com"
- **project_name**: "Project name"
- **description**: "A short description of the project."
- **repo_name**: "{project-name}-{author-name}"
- **dev_env_name**: "{project-name}-denv"

[cookiecutter_docs]: http://cookiecutter.readthedocs.org/en/latest/installation.html
[pixi_docs]: https://github.com/prefix-dev/pixi
